# Bank Simulator
- Bank Simulator test project
- An app to simulate a bank by creating accounts, and making transactions.
- It runs on Spring Boot with H2 Database

## Install notes
1. install eclipse: m2e-apt plugin
    - Help > Eclipse Marketplace ... search m2e-apt
    - eclipse enable annotation processor - https://manios.org/2017/08/09/configure-eclipse-in-order-to-build-mapstruct-in-java-projects
3. copy/use `src\main\resources\static\lombok-1.18.10--withMapstruct.jar` to `<eclipse>\lombok-1.18.10--withMapstruct.jar`
4. Edit `<eclipse>\eclipse.ini` the following last line `-javaagent:lombok-1.18.10--withMapstruct.jar`
5. Download sources and run the app. 
    - Check **BASE_URL** = `http://localhost:8089`

## Entrypoints
- All entrypoints need Basic Authentication
  - user: `admin`
  - pass: `admin2`

### Accounts: ACC_URL: `BASE_URL/api/banksim/accounts`
- List accounts
  - with `GET` request, at **ACC_URL**
- Create account
  - with `POST` request, at **ACC_URL**, add body (account form):
	```
	{
      "surname": "Popescu",
      "lastName": "Ion",
      "address": "Str Sarii 30",
      "cnp": "1930415262345"
	}
	```
  - app validates form if it's not filled correctly
    - ex: `cnp` must be 13 length and only DIGITS
- Update account. Only status can be changed but is **open for extension**
  - with `PUT` request, at `ACC_URL/{accountId}`, fill form:
    ```
    {
      "changeType": "statusChange",
      "changeValue": "BLOCKED"
    }
    ```
    
### Transactions: TRNS_URL: `BASE_URL/api/banksim/transactions`
- List transactions
  - with `GET` request, at **TRNS_URL**
  - filter transactions adding query parameters `TRNS_URL?`:
    - `accountId` - if mentioned, it filters only the transactions for the account specified, otherwise all are retrieved
    - `pastTimeFrame` - select from:
      - "lastMinute"
      - "lastHour"
      - "last6Hours"
      - "last24Hours"
      - "last3Days"
      - "last7Days"
      - "last30Days":
    - if `pastTimeFrame` is empty
      - give an encoded OffsetDateTime type from UI. example for: `2020-11-01 18:19:27.113+02` to `2020-11-03 18:19:27.113+02`
      - `timeFrameFrom=2020-10-20%2018%3A19%3A27.113%2B02`
      - `timeFrameTo=2020-10-25%2018%3A19%3A27.113%2B02`
- Create transaction
  - with `POST` request, at **TRNS_URL**, add body (transaction form):
	```
	{
      "accountId": 103,
      "transactionType": "DEPOSIT", (select from DEPOSIT/WITHDRAWAL)
      "accountMovement": 450,
      "description": "alimentare cont"
	}
	```
