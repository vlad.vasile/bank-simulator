package com.banksim.common;

public enum AccountStatus {
  ACTIVE,
  BLOCKED,
  INACTIVE,;

  public static AccountStatus from(String statusValue) {
    return AccountStatus.valueOf(statusValue);
  }
}
