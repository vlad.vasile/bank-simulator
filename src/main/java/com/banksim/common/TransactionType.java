package com.banksim.common;

public enum TransactionType {
  WITHDRAWAL,
  DEPOSIT
}
