package com.banksim.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.banksim.model.AccountModel;
import com.banksim.rest.GenericController;
import com.banksim.rest.RestResponse;
import com.banksim.service.BankService;

import io.vavr.control.Either;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/banksim/accounts")
public class AccountController extends GenericController {

  @Autowired
  private BankService bankService;

  @RequestMapping(method = RequestMethod.GET)
  ResponseEntity<RestResponse<List<AccountModel>>> getAllAccounts() {
    List<AccountModel> accounts = bankService.getAllAccounts();
    log.info("Retrieving {} accounts", accounts.size());
    return ok("get accounts", accounts);
  }

  @RequestMapping(method = RequestMethod.POST)
  ResponseEntity<RestResponse<AccountModel>> createAccount(@RequestBody AccountForm accountForm) {
    AccountModel accountModel = bankService.createAccount(accountForm.validateRequiredFields());
    log.info("Created account {}", accountModel);
    return ok("get accounts", accountModel);
  }

  @RequestMapping(method = RequestMethod.PUT, path = "/{id}")
  ResponseEntity<RestResponse<AccountModel>> updateAccount(@PathVariable(name = "id") Long accountId,
      @RequestBody ConfigAccount accountUpdater) {
    accountUpdater = accountUpdater.withAccountId(accountId);
    log.info("Update account: {}", accountUpdater);
    Either<AccountModel, String> account = bankService.updateAccount(accountUpdater);
    if (account.isLeft()) {
      return ok("get accounts", account.getLeft());
    } else {
      return error(HttpStatus.NOT_FOUND, account.swap().getLeft());
    }
  }
}
