package com.banksim.controller;

import com.google.common.base.Preconditions;

public class AccountForm {
  private static final String ANY_DIGIT_REGEX = "\\d+";
  public String surname;
  public String lastName;
  public String address;
  public String cnp;

  public AccountForm validateRequiredFields() {
    Preconditions.checkNotNull(surname, "Surname not provided");
    Preconditions.checkNotNull(lastName, "Last name not provided");
    Preconditions.checkNotNull(address, "Address not provided");
    Preconditions.checkNotNull(cnp, "CNP not provided");
    Preconditions.checkArgument(cnp.matches(ANY_DIGIT_REGEX), "CNP must contain only digits!");
    Preconditions.checkArgument(cnp.length() == 13, "CNP must be 13 digits long");
    return this;
  }
}
