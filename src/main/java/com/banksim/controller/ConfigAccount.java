package com.banksim.controller;

import com.banksim.common.AccountStatus;
import com.banksim.domain.Account;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConfigAccount {
  public Long accountId;
  public String changeType;
  public String changeValue;

  public Account update(Account account) {
    if (account == null) {
      return null;
    }
    switch (changeType) {
      case "statusChange":
        AccountStatus status = AccountStatus.from(changeValue);
        account = account.withStatus(status);
        break;
      // can add different cases which are sent from UI
      default:
        break;
    }
    return account;
  }
}