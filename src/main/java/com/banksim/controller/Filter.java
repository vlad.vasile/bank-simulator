package com.banksim.controller;

import java.time.OffsetDateTime;

import com.banksim.domain.Transaction;
import com.google.common.base.Strings;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter(lombok.AccessLevel.NONE)
@Setter(lombok.AccessLevel.NONE)
@Builder
public class Filter {
  public Long accountId;
  public TimeFrame timeFrame;

  public boolean accept(Transaction transaction) {
    boolean accountIdCheck = true;
    if (accountId != null) {
      accountIdCheck = transaction.hasAccountId(accountId);
    }
    return accountIdCheck && transaction.isBetweenTimeFrames(timeFrame);
  }

  public static Filter from(Long accountId, String pastTimeFrame, String timeFrameFrom, String timeFrameTo) {
    return Filter.builder()
      .accountId(accountId)
      .timeFrame(TimeFrame.from(pastTimeFrame, timeFrameFrom, timeFrameTo))
      .build();
  }

  @Data
  @Getter(lombok.AccessLevel.NONE)
  @Setter(lombok.AccessLevel.NONE)
  @AllArgsConstructor(access = AccessLevel.PRIVATE)
  @NoArgsConstructor(access = AccessLevel.PRIVATE)
  public static class TimeFrame {
    private final static int DEFAULT_TIME_FRAME_FROM_YEARS = 5;
    public OffsetDateTime timeFrameFrom;
    public OffsetDateTime timeFrameTo;

    public static TimeFrame from(String pastTimeFrame, String timeFrameFrom, String timeFrameTo) {
      return defaultsIfEmpty(pastTimeFrame, timeFrameFrom, timeFrameTo);
    }

    private static TimeFrame defaultsIfEmpty(String requestPastTimeFrame, String requestTimeFrameFrom,
        String requestTimeFrameTo) {
      OffsetDateTime timeFrameFrom;
      OffsetDateTime timeFrameTo = OffsetDateTime.now();
      if (!Strings.isNullOrEmpty(requestPastTimeFrame)) {
        switch (requestPastTimeFrame) {
          case "lastMinute":
            timeFrameFrom = OffsetDateTime.now().minusMinutes(1);
            break;
          case "lastHour":
            timeFrameFrom = OffsetDateTime.now().minusHours(1);
            break;
          case "last6Hours":
            timeFrameFrom = OffsetDateTime.now().minusHours(6);
            break;
          case "last24Hours":
            timeFrameFrom = OffsetDateTime.now().minusHours(24);
            break;
          case "last3Days":
            timeFrameFrom = OffsetDateTime.now().minusDays(3);
            break;
          case "last7Days":
            timeFrameFrom = OffsetDateTime.now().minusDays(7);
            break;
          case "last30Days":
            timeFrameFrom = OffsetDateTime.now().minusDays(30);
            break;
          default:
            throw new RuntimeException("Past time frame unreadable: [" + requestPastTimeFrame + "]");
        }
      } else {
        if (Strings.isNullOrEmpty(requestTimeFrameFrom)) {
          timeFrameFrom = OffsetDateTime.now().minusYears(DEFAULT_TIME_FRAME_FROM_YEARS);
        } else {
          timeFrameFrom = OffsetDateTime.parse(requestTimeFrameFrom);
        }
        if (Strings.isNullOrEmpty(requestTimeFrameTo)) {
          timeFrameTo = OffsetDateTime.now();
        } else {
          timeFrameTo = OffsetDateTime.parse(requestTimeFrameTo);
        }
      }

      return new TimeFrame(timeFrameFrom, timeFrameTo);
    }

    public boolean isBetweenTimeFrames(OffsetDateTime date) {
      return date.isBefore(timeFrameTo) && date.isAfter(timeFrameFrom);
    }
  }
}