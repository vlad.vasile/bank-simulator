package com.banksim.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.banksim.model.TransactionModel;
import com.banksim.rest.GenericController;
import com.banksim.rest.RestResponse;
import com.banksim.service.BankService;

import io.vavr.control.Either;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/banksim/transactions")
public class TransactionController extends GenericController {
  @Autowired
  private BankService bankService;

  @RequestMapping(method = RequestMethod.GET)
  ResponseEntity<RestResponse<List<TransactionModel>>> getAllTransactions(
      @RequestParam(name = "accountId") Long accountId,
      @RequestParam(name = "pastTimeFrame") String pastTimeFrame,
      @RequestParam(name = "timeFrameFrom") String timeFrameFrom,
      @RequestParam(name = "timeFrameTo") String timeFrameTo) {

    Filter filter = Filter.from(accountId, pastTimeFrame, timeFrameFrom, timeFrameTo);
    List<TransactionModel> transactions = bankService.findTransactions(filter);
    log.info("Retrieving {} transactions", transactions.size());
    return ok("get transactions", transactions);
  }

  @RequestMapping(method = RequestMethod.POST)
  ResponseEntity<RestResponse<TransactionModel>> createTransaction(@RequestBody TransactionForm transactionForm) {
    Either<TransactionModel, String> transactionResponse = bankService
      .createTransaction(transactionForm.validateAndFix());
    if (transactionResponse.isRight()) {
      return error(HttpStatus.NOT_FOUND, transactionResponse.get());
    }
    log.info("New transaction created: {}", transactionResponse.getLeft());
    return ok("new transaction", transactionResponse.getLeft());
  }

}
