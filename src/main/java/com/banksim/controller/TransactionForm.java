package com.banksim.controller;

import java.math.BigDecimal;

import com.banksim.common.TransactionType;
import com.banksim.domain.Account;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter(AccessLevel.NONE)
@Setter(AccessLevel.NONE)
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TransactionForm {
  public Long accountId;
  public TransactionType transactionType;
  public BigDecimal accountMovement;
  public String description;

  public TransactionForm validateAndFix() {
    Preconditions.checkNotNull(accountId, "Account ID missing.");
    Preconditions.checkNotNull(transactionType, "Select transaction type.");
    Preconditions.checkNotNull(accountMovement, "Select amount.");
    if (Strings.isNullOrEmpty(description)) {
      // auto fill description if not given
      this.description = transactionType.toString();
    }
    if (transactionType == TransactionType.WITHDRAWAL) {
      accountMovement = accountMovement.negate();
    }
    return this;
  }

  public Account updateAccount(Account account) {
    BigDecimal balanceAfter = account.balance.add(accountMovement);
    account = account.withBalance(balanceAfter);
    return account;
  }
}
