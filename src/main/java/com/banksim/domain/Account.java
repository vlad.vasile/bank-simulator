package com.banksim.domain;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;

import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.CreationTimestamp;

import com.banksim.common.AccountStatus;

import lombok.AccessLevel;

//https://stackoverflow.com/questions/34241718/lombok-builder-and-jpa-default-constructor
//Mandatory in conjunction with JPA: an equal based on fields is not desired
@lombok.EqualsAndHashCode(onlyExplicitlyIncluded = true)
//Mandatory in conjunction with JPA: force is needed to generate default values for final fields, that will be
//overriden by JPA
@lombok.NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
//Hides the constructor to force usage of the Builder.
@lombok.AllArgsConstructor(access = AccessLevel.PRIVATE)
@lombok.ToString
//Good to just modify some values
@lombok.With
//Mandatory in conjunction with JPA: Some suggest that the Builder should be above Entity -
//https://stackoverflow.com/a/52048267/99248
//Good to be used to modify all values
@lombok.Builder(toBuilder = true)
//final fields needed for imutability, the default access to public - since are final is safe
@lombok.experimental.FieldDefaults(makeFinal = true, level = AccessLevel.PUBLIC)
//no getters and setters
@lombok.Getter(value = AccessLevel.NONE)
@lombok.Setter(value = AccessLevel.NONE)

//JPA
@javax.persistence.Entity
@javax.persistence.Table(name = "ACCOUNT", uniqueConstraints = {
    @UniqueConstraint(columnNames = { "CNP" })
})
//jpa should use field access
@javax.persistence.Access(AccessType.FIELD)
public class Account {
  @lombok.EqualsAndHashCode.Include
  @Id
  @GeneratedValue
  Long id;
  @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL")
  @CreationTimestamp
  OffsetDateTime created;
  String surname;
  String lastName;
  String address;
  AccountStatus status;
  BigDecimal balance;
  String cnp;
  @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "accountId")
  List<Transaction> transactions;

  public Account updateStartingValues() {
    return withStatus(AccountStatus.ACTIVE).withBalance(BigDecimal.ZERO);
  }

  public boolean isActive() {
    return status != null && status == AccountStatus.ACTIVE;
  }

  public boolean hasSufficientFonds(BigDecimal accountMovement) {
    BigDecimal balance2 = balance;
    return balance2.add(accountMovement).compareTo(BigDecimal.ZERO) >= 0;
  }

  public String transactionCode() {
    return String.format("%s-%s", id, transactions.size() + 1);
  }
}
