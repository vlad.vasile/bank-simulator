package com.banksim.mapper;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.banksim.controller.AccountForm;
import com.banksim.controller.TransactionForm;
import com.banksim.domain.Account;
import com.banksim.domain.Transaction;
import com.banksim.model.AccountModel;
import com.banksim.model.TransactionModel;

@Mapper(config = BankSimMapstructConfig.class)
public interface GeneralMapper {
  GeneralMapper mapper = Mappers.getMapper(GeneralMapper.class);

  List<AccountModel> toAccountModels(List<Account> accounts);

  AccountModel toAccountModel(Account account);

  Account toAccount(AccountModel accountModel);

  List<TransactionModel> toTransactionModels(List<Transaction> transactions);

  TransactionModel toTransactionModel(Transaction transaction);

  Transaction toAccount(TransactionModel transactionModel);

  @Mapping(ignore = true, target = "id")
  @Mapping(source = "transactionForm.accountId", target = "accountId")
  @Mapping(source = "transactionForm.transactionType", target = "type")
  @Mapping(source = "account.balance", target = "balanceBefore")
  @Mapping(expression = "java(account.transactionCode())", target = "code")
  @Mapping(expression = "java(changeBalance(account.balance, transactionForm.accountMovement))",
      target = "balanceAfter")
  @Mapping(expression = "java(creationTime())", target = "created")
  Transaction toNewTransaction(Account account, TransactionForm transactionForm);

  default BigDecimal changeBalance(BigDecimal actualBalance, BigDecimal accountMovement) {
    return actualBalance.add(accountMovement);
  }

  default OffsetDateTime creationTime() {
    return OffsetDateTime.now();
  }

  Account toNewAccount(AccountForm accountForm);
}
