package com.banksim.model;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;

import com.banksim.common.AccountStatus;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter(AccessLevel.NONE)
@Setter(AccessLevel.NONE)
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AccountModel {
  public Long id;
  public OffsetDateTime created;
  public String surname;
  public String lastName;
  public String address;
  public AccountStatus status;
  public BigDecimal balance;
  public String cnp;
  public List<TransactionModel> transactions;
}
