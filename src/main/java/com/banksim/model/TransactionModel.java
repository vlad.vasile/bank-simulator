package com.banksim.model;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

import com.banksim.common.TransactionType;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter(AccessLevel.NONE)
@Setter(AccessLevel.NONE)
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TransactionModel {
  public Long id;
  public String code;
  public Long accountId;
  public OffsetDateTime created;
  public TransactionType type;
  public BigDecimal balanceBefore;
  public BigDecimal balanceAfter;
  public BigDecimal accountMovement;
  public String description;
}
