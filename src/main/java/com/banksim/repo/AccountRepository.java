package com.banksim.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.banksim.domain.Account;

@Component
public interface AccountRepository extends JpaRepository<Account, Long> {
  boolean existsByCnp(String cnp);

  Account findByCnp(String cnp);
}
