package com.banksim.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.banksim.domain.Transaction;

@Component
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

}
