package com.banksim.repo.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.banksim.repo.AccountRepository;
import com.banksim.repo.TransactionRepository;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class BankSimInitDatabase {

  @Bean
  CommandLineRunner initDatabase(AccountRepository accountRepo, TransactionRepository transactionRepo) {
    return args -> {
      log.info("INIT AND SEED REVOBET DATABASE !!!!");
      try {
        //        Account sampleAccount = Account.builder()
        //          .surname("Vlad")
        //          .lastName("Vasile")
        //          .address("Str Opanez 2")
        //          .balance(BigDecimal.ZERO)
        //          .cnp("1910703170058L)")
        //          .created(OffsetDateTime.now())
        //          .isActive(true)
        //          .build();
        //        if (!accountRepo.existsByCnp(sampleAccount.cnp)) {
        //          accountRepo.save(sampleAccount);
        //        }
        //        Account dbAccount = accountRepo.findByCnp(sampleAccount.cnp);
        //
        //        Transaction transactionSample = Transaction.builder()
        //          .accountId(dbAccount.id)
        //          .accountMovement(BigDecimal.TEN)
        //          .balanceBefore(BigDecimal.ZERO)
        //          .balanceAfter(BigDecimal.TEN)
        //          .created(OffsetDateTime.now())
        //          .description("alimentare cont")
        //          .build();
        //
        //        transactionRepo.save(transactionSample);
      } catch (Exception e) {
        log.warn("Couldn't initialize database with sample data.", e);
      }
    };
  }
}
