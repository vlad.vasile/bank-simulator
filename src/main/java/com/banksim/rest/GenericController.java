package com.banksim.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class GenericController {

  public <T> ResponseEntity<RestResponse<T>> ok(String string, String version, T payload) {
    RestResponse<T> response = RestResponse.<T>builder()
      .success(true)
      .status(HttpStatus.OK.value())
      .message(string)
      .payload(payload)
      .version(version)
      .build();
    return ResponseEntity.ok(response);
  }

  public <T> ResponseEntity<RestResponse<T>> error(final HttpStatus status, final String message, String version,
      T payload) {
    RestResponse<T> response = RestResponse.<T>builder()
      .success(false)
      .status(status.value())
      .message(message)
      .version(version)
      .payload(payload)
      .build();
    return ResponseEntity.status(response.status).body(response);
  }

  public <T> ResponseEntity<RestResponse<T>> ok(String string, T payload) {
    RestResponse<T> response = RestResponse.<T>builder()
      .success(true)
      .status(HttpStatus.OK.value())
      .message(string)
      .payload(payload)
      .build();
    return ResponseEntity.ok(response);
  }

  public <T> ResponseEntity<RestResponse<T>> error(final HttpStatus status, final String message) {
    RestResponse<T> response = RestResponse.<T>builder()
      .success(false)
      .status(status.value())
      .message(message)
      .build();
    return ResponseEntity.status(response.status).body(response);
  }
}