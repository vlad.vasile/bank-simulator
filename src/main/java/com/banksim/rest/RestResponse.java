package com.banksim.rest;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter(value = AccessLevel.NONE)
@Setter(value = AccessLevel.NONE)
@Builder
public class RestResponse<T> {

  public boolean success;

  public int status;

  public Exception error;

  public String message;

  public T payload;

  public String version = "0";
}
