package com.banksim.service;

import java.util.List;
import java.util.Optional;

import com.banksim.controller.ConfigAccount;
import com.banksim.controller.TransactionForm;
import com.banksim.domain.Account;

import io.vavr.control.Either;

public interface AccountService {

  List<Account> getAllAccounts();

  Account createAccount(Account account);

  Optional<Account> updateAccount(ConfigAccount accountUpdater);

  Optional<Account> findById(Long accountId);

  Either<Account, String> validateAccountForTransaction(TransactionForm transactionForm);

  Account save(Account updatedAccount);

}
