package com.banksim.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.banksim.controller.ConfigAccount;
import com.banksim.controller.TransactionForm;
import com.banksim.domain.Account;
import com.banksim.repo.AccountRepository;

import io.vavr.control.Either;

@Component
public class AccountServiceImpl implements AccountService {
  @Autowired
  private AccountRepository accountRepo;

  @Override
  public List<Account> getAllAccounts() {
    return accountRepo.findAll();
  }

  @Override
  public Account createAccount(Account account) {
    account = account.updateStartingValues();
    return accountRepo.save(account);
  }

  @Override
  public Optional<Account> updateAccount(ConfigAccount configAccount) {
    return accountRepo.findById(configAccount.accountId)
      .map(x -> updateAndSave(configAccount, x));
  }

  private Account updateAndSave(ConfigAccount configAccount, Account account) {
    Account updatedAccount = configAccount.update(account);
    return accountRepo.save(updatedAccount);
  }

  @Override
  public Optional<Account> findById(Long accountId) {
    return accountRepo.findById(accountId);
  }

  @Override
  public Either<Account, String> validateAccountForTransaction(TransactionForm transactionForm) {
    Optional<Account> accountOpt = findById(transactionForm.accountId);
    if (!accountOpt.isPresent()) {
      return Either.right("Account with ID: " + transactionForm.accountId + " not found!");
    }
    Account account = accountOpt.get();
    if (!account.isActive()) {
      return Either.right("Account with ID: " + transactionForm.accountId
          + " is not eligible for transactions. Status: " + account.status);
    }
    if (!account.hasSufficientFonds(transactionForm.accountMovement)) {
      return Either.right("Insufficient fonds. Actual balance: " + account.balance);
    }
    return Either.left(account);

  }

  @Override
  public Account save(Account account) {
    return accountRepo.save(account);
  }

}
