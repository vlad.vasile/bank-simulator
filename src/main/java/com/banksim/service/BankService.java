package com.banksim.service;

import java.util.List;

import com.banksim.controller.AccountForm;
import com.banksim.controller.ConfigAccount;
import com.banksim.controller.Filter;
import com.banksim.controller.TransactionForm;
import com.banksim.model.AccountModel;
import com.banksim.model.TransactionModel;

import io.vavr.control.Either;

public interface BankService {
  List<AccountModel> getAllAccounts();

  AccountModel createAccount(AccountForm accountForm);

  Either<AccountModel, String> updateAccount(ConfigAccount withAccountId);

  List<TransactionModel> findTransactions(Filter filter);

  Either<TransactionModel, String> createTransaction(TransactionForm validate);
}
