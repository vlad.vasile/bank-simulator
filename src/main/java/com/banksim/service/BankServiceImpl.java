package com.banksim.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.banksim.controller.AccountForm;
import com.banksim.controller.ConfigAccount;
import com.banksim.controller.Filter;
import com.banksim.controller.TransactionForm;
import com.banksim.domain.Account;
import com.banksim.domain.Transaction;
import com.banksim.mapper.GeneralMapper;
import com.banksim.model.AccountModel;
import com.banksim.model.TransactionModel;

import io.vavr.control.Either;

@Component
public class BankServiceImpl implements BankService {
  private static final GeneralMapper mapper = GeneralMapper.mapper;
  @Autowired
  private AccountService accountService;
  @Autowired
  private TransactionService transactionService;

  @Override
  public List<AccountModel> getAllAccounts() {
    List<Account> accounts = accountService.getAllAccounts();
    return toAccountModels(accounts);
  }

  @Override
  public AccountModel createAccount(AccountForm accountForm) {
    Account account = toAccount(accountForm);
    return toAccountModel(accountService.createAccount(account));
  }

  private Account toAccount(AccountForm accountForm) {
    return mapper.toNewAccount(accountForm);
  }

  @Override
  public Either<AccountModel, String> updateAccount(ConfigAccount accountUpdater) {
    Optional<Account> updatedAccountOpt = accountService.updateAccount(accountUpdater);
    if (updatedAccountOpt.isPresent()) {
      return Either.left(toAccountModel(updatedAccountOpt.get()));
    } else {
      return Either.right(notFoundMessage(accountUpdater));
    }
    //      .map(x -> {
    //        return Either.left(toAccountModel(x));
    //      })
    //      .orElseGet(() -> Either.right(notFoundMessage(accountUpdater)));
  }

  private AccountModel toAccountModel(Account account) {
    return mapper.toAccountModel(account);
  }

  private List<AccountModel> toAccountModels(List<Account> accounts) {
    return mapper.toAccountModels(accounts);
  }

  private String notFoundMessage(ConfigAccount accountUpdater) {
    return "Account with id " + accountUpdater.accountId + " not found";
  }

  @Override
  public List<TransactionModel> findTransactions(Filter filter) {
    List<Transaction> transactions = io.vavr.collection.List.ofAll(transactionService.getAllTransactions())
      .filter(transaction -> filter.accept(transaction))
      .asJava();
    return toTransactionModels(transactions);
  }

  private List<TransactionModel> toTransactionModels(List<Transaction> transactions) {
    return mapper.toTransactionModels(transactions);
  }

  @Override
  public Either<TransactionModel, String> createTransaction(TransactionForm transactionForm) {
    Either<Account, String> accountEither = accountService.validateAccountForTransaction(transactionForm);
    if (accountEither.isRight()) {
      return Either.right(accountEither.get());
    }
    Transaction transaction = updateAccountAndSaveTransaction(transactionForm, accountEither);
    return Either.left(mapper.toTransactionModel(transaction));
  }

  private Transaction updateAccountAndSaveTransaction(TransactionForm transactionForm,
      Either<Account, String> accountEither) {
    Account account = accountEither.getLeft();
    Account updatedAccount = transactionForm.updateAccount(account);
    Transaction transaction = transactionService.createTransaction(account, transactionForm);
    accountService.save(updatedAccount);
    return transaction;
  }

}
