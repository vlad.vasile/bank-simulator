package com.banksim.service;

import java.util.List;

import com.banksim.controller.TransactionForm;
import com.banksim.domain.Account;
import com.banksim.domain.Transaction;

public interface TransactionService {

  List<Transaction> getAllTransactions();

  Transaction createTransaction(Account account, TransactionForm transactionForm);

}
