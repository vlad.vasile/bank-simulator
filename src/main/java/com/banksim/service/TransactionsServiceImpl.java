package com.banksim.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.banksim.controller.TransactionForm;
import com.banksim.domain.Account;
import com.banksim.domain.Transaction;
import com.banksim.mapper.GeneralMapper;
import com.banksim.repo.TransactionRepository;

@Component
public class TransactionsServiceImpl implements TransactionService {
  @Autowired
  private TransactionRepository transactionRepo;

  @Override
  public List<Transaction> getAllTransactions() {
    return transactionRepo.findAll();
  }

  @Override
  public Transaction createTransaction(Account account, TransactionForm transactionForm) {
    Transaction newTransaction = GeneralMapper.mapper.toNewTransaction(account, transactionForm);
    return transactionRepo.save(newTransaction);
  }
}
